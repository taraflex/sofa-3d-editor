const fs = require('fs');

function genModels() {
    return new Array(4).fill(0).map((e, i) => ({
        name: 'test' + i,
        url: `/models/${i}.FBX`,
        preview: 'https://loremflickr.com/100/100/dog?lock=' + i,//'/models/preview/' + i,
        price: i
    })).slice(1);
}

function genMaterials(g) {
    return new Array(100).fill(0).map((e, i) => {
        i += g * 100;
        return { name: 'texture' + i, url: 'https://loremflickr.com/1024/1024?lock=' + i, preview: 'https://loremflickr.com/100/100?lock=' + i };
    });
}

function genMaterialGroups() {
    return new Array(10).fill(0).map((e, i) => ({
        name: 'group' + i,
        preview: 'https://placekitten.com/100/100?image=' + i,
        data: genMaterials(i)
    }))
}

function gen() {
    return [
        { name: 'Models', data: genModels() },
        { name: 'Materials', data: genMaterialGroups() }
    ]
}
fs.writeFileSync(__dirname + '/src/data.json', JSON.stringify(gen(), null, 4))