import { Mesh, MeshBasicMaterial, PlaneBufferGeometry } from 'three';

const geom = new PlaneBufferGeometry(1, 1, 4, 4);
geom.computeBoundingBox();

const material = new MeshBasicMaterial({
    color: 'black',
    fog: false,
    depthWrite: false,
    depthTest: false
})

export class Shadow extends Mesh {
    constructor() {
        super(geom, material);
        this.frustumCulled = true;
        this.castShadow = false;
        this.receiveShadow = false;
        this.rotateX(-Math.PI / 2);
    }
    set width(value: number) {
        this.scale.x = value;
    }
    set height(value: number) {
        this.scale.y = value;
    }
}