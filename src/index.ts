import 'normalize.css';
import 'core-js/stable/promise';
import 'core-js/stable/array';
import 'whatwg-fetch';

import wdr from 'when-dom-ready';

//@ts-ignore
import App from './App.svelte';

wdr().then(() => new App({ target: document.body })).catch(console.error.bind(console));