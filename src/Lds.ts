import genPixel from 'gen-pixel';
import { Shadow } from 'Shadow';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';
import {
    Box3, Color, FileLoader, Group, LinearFilter, Mesh, MeshMatcapMaterial, Object3D,
    RepeatWrapping, Sprite, SpriteMaterial, Texture, TextureLoader, Vector3
} from 'three';

class AjaxTextureLoader extends TextureLoader {
    load(
        url: string,
        onLoad?: (texture: Texture) => void,
        onProgress?: (event: ProgressEvent) => void,
        onError?: (event: ErrorEvent) => void
    ) {
        // Cache.enabled = true;
        const fileLoader = new FileLoader();
        fileLoader.setResponseType('blob');
        fileLoader.load(url, (blob) => {
            // ObjectURLs should be released as soon as is safe, to free memory
            const objUrl = URL.createObjectURL(blob);
            const image = document.createElementNS('http://www.w3.org/1999/xhtml', 'img') as HTMLImageElement;
            image.onload = () => {
                // Cache.add(url, image);
                URL.revokeObjectURL(objUrl);
                document.body.removeChild(image);
                super.load(url, onLoad, undefined, onError);
            };
            image.src = objUrl;
            image.hidden = true;
            document.body.appendChild(image);
        }, onProgress, onError);
        return null;
    }
}

export type Info = {
    url: string,
    preview: string,
    name: string,
    path: number[]
}

const SimpleTextureLoader = new TextureLoader();

function createButtonTexture(url: string) {
    const t = SimpleTextureLoader.load(url);
    t.generateMipmaps = false;
    t.minFilter = t.magFilter = LinearFilter;
    t.needsUpdate = true;
    return t;
}

function createButtonMaterial(map: Texture, rotation = 0) {
    return new SpriteMaterial({
        map,
        sizeAttenuation: false,
        depthTest: false,
        depthWrite: false,
        fog: false,
        transparent: true,
        alphaTest: 0.1,
        rotation
    });
}

const xtexture = createButtonTexture(require('./icons/x-circle.png').default);
const plusPointMaterial = createButtonMaterial(xtexture, Math.PI / 4);
const removePointMaterial = createButtonMaterial(xtexture);
const rotatePointMaterial = createButtonMaterial(createButtonTexture(require('./icons/replay_circle_filled.png').default));
const colorPointMaterial = createButtonMaterial(createButtonTexture(require('./icons/color_lens.png').default));
const replacePointMaterial = createButtonMaterial(createButtonTexture(require('./icons/arrow-repeat.png').default));

export const enum Layers {
    BUTTONS = 2,
    BBOX = 4,
    SHADOW = 8,
}

function createButtton(parent: Object3D, material: SpriteMaterial, name: string) {
    const point = new Sprite(material);
    point.scale.set(0.08, 0.08, 1);
    point.layers.enable(Layers.BUTTONS);
    point.name = name;
    point.renderOrder = Number.MAX_SAFE_INTEGER;
    parent.add(point);
    return point;
}

export class Lds {
    private readonly cache: Record<string, any> = Object.create(null);
    private readonly textureLoader: AjaxTextureLoader;
    private readonly modelLoader: FBXLoader;
    private material: MeshMatcapMaterial;

    static isMesh(o: Object3D) {
        return (o as Mesh).isMesh && !o.name.startsWith('_');
    }

    static appendTexture(object: Object3D, texture: Texture) {
        if (texture) {
            let m: MeshMatcapMaterial;
            object.traverse((o: Mesh) => {
                if (Lds.isMesh(o) && texture != (o.material as MeshMatcapMaterial).map) {
                    if (!m) {
                        m = (o.material as MeshMatcapMaterial).clone();
                        m.map = texture;
                        m.needsUpdate = true;
                    }
                    o.material = m;
                }
            });
        }
    }

    static deepClone(object: Object3D, texture: Texture) {
        const cloned = object.clone();
        Lds.appendTexture(cloned, texture);
        return cloned;
    }

    processModel(object: Object3D) {
        const markForRemove = [];
        object.traverse((o: Mesh) => {
            if (o.name.startsWith('_pp')) {

                const g = new Group();
                g.rotation.copy(o.rotation);
                g.position.setFromMatrixPosition(o.matrix);
                g.name = o.name;

                createButtton(g, plusPointMaterial, 'plus');

                o.parent.add(g);

                markForRemove.push(o);
            } else if (o.name.startsWith('_bb')) {
                o.layers.set(Layers.BBOX);
                (o as Mesh).geometry.computeBoundingBox();
                o.visible = true;
                if (o.name === '_bb') {
                    const sh = new Shadow();
                    sh.layers.set(Layers.SHADOW);
                    sh.name = '_shadow';
                    o.parent.add(sh);
                    const { x, z } = new Box3().setFromObject(o).getSize(new Vector3());
                    sh.width = x;
                    sh.height = z;
                    sh.position.set(o.position.x, 0, o.position.z);
                }
            } else if (Lds.isMesh(o)) {
                o.geometry.computeBoundingBox();
                o.material = this.material;
            }
        });

        markForRemove.forEach(o => o.parent.remove(o));

        const removePoint = createButtton(object, removePointMaterial, 'remove');
        removePoint.position.set(0, 0.7, 0);
        removePoint.center.set(0 + 1, 0);

        const rotatePoint = createButtton(object, rotatePointMaterial, 'rotate');
        rotatePoint.position.set(0, 0.7, 0);
        rotatePoint.center.set(-0.69 * 1.4 + 1, 0.69 * 1.4);

        const replacePoint = createButtton(object, replacePointMaterial, 'replace');
        replacePoint.position.set(0, 0.7, 0);
        replacePoint.center.set(-0.69 * 2 + 1, 0.69 * 3);

        const colorPoint = createButtton(object, colorPointMaterial, 'color');
        colorPoint.position.set(0, 0.7, 0);
        colorPoint.center.copy(replacePoint.center);

        return object;
    }

    constructor(private readonly onProgress: (event: ProgressEvent) => void) {
        this.textureLoader = new AjaxTextureLoader();
        this.modelLoader = new FBXLoader()
    }

    private progress(start: boolean) {
        this.onProgress(new ProgressEvent('progress', { loaded: start ? 0 : 1, total: 1 }));
    }

    loadModel(info: Info, texture: Texture): Promise<Object3D> {
        const { url } = info;
        return this.cache[url] ? Promise.resolve(Lds.deepClone(this.cache[url], texture)) : new Promise((resolve, reject) => {
            this.progress(true);
            const mc = !this.material && this.loadTexture({ url: require('./matcap.jpg').default } as any, SimpleTextureLoader);
            this.modelLoader.load(url, async t => {
                if (mc) {
                    this.material = new MeshMatcapMaterial({
                        matcap: await mc,
                        map: SimpleTextureLoader.load(genPixel(new Color(230, 230, 230)))
                    });
                }
                t.userData = info;
                resolve(Lds.deepClone(this.cache[url] = this.processModel(t), texture));
            }, this.onProgress, reject);
        });
    }

    loadTexture(info: Info, loader?: TextureLoader): Promise<Texture> {
        const { url } = info;
        return this.cache[url] ? Promise.resolve(this.cache[url]) : new Promise((resolve, reject) => {
            this.progress(true);
            (loader || this.textureLoader).load(url, t => {
                t.wrapS = t.wrapT = RepeatWrapping;
                t.needsUpdate = true;
                t['userData'] = info;
                resolve(this.cache[url] = t);
            }, this.onProgress, reject)
        });
    }

    disposeTexture(t: Texture) {
        this.cache[t['userData'].url] = undefined;
        t.dispose();
    }
}