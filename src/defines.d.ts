declare const APP_NAME: string;
declare const APP_VERSION: string;

declare module '*.pug' {
    export default function (locals?: Record<string, any>): string;
}

declare module '*.txt' {
    const t: string;
    export default t;
}

declare module '*.glsl' {
    const t: string;
    export default t;
}