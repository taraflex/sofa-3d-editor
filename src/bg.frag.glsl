#include <common>
#define ITERATIONS 5.0
uniform sampler2D shadow;
varying vec2 vUv;

vec3 blend(vec3 old, vec3 new, float alpha){
    return alpha * new + (1.0 - alpha) * old;
}

void main() {
    vec2 delta = vec2( 0.2, 0 );
    vec4 color = vec4( 0.0 );
	float total = 0.0;
	float offset = rand( vUv  );

	for ( float t = -ITERATIONS; t <= ITERATIONS; t ++ ) {
		float percent = ( t + offset - 0.5) / ITERATIONS;
		float weight = 1.0 - abs( percent );
		color += texture2D( shadow, vUv - delta * percent ) * weight;
		total += weight;
	}
	
	vec4 t = color / total;
    //float c = mix( 0.8, 0.98, vUv.y );
    gl_FragColor = vec4(blend(vec3(0.985), t.xyz, t.w * 0.3), 1.0);
}