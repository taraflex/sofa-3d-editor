import { Color } from "three";

function encodeRGB(r: number, g: number, b: number) {
    return encode_triplet(0, r, g) + encode_triplet(b, 255, 255);
}

const keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

function encode_triplet(e1: number, e2: number, e3: number) {
    const enc1 = e1 >> 2;
    const enc2 = ((e1 & 3) << 4) | (e2 >> 4);
    const enc3 = ((e2 & 15) << 2) | (e3 >> 6);
    const enc4 = e3 & 63;
    return keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);
}

export default function (color: Color) {
    return "data:image/gif;base64,R0lGODlhAQABAPAA" + encodeRGB(color.r, color.g, color.b) + "/yH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==";
}