import { Mesh, NoBlending, PlaneBufferGeometry, ShaderMaterial, Texture } from 'three';

import fragmentShader from './bg.frag.glsl';
import vertexShader from './bg.vert.glsl';

export class Background extends Mesh {
    constructor(shadowTexture: Texture) {
        super(new PlaneBufferGeometry(2, 2, 1, 1), new ShaderMaterial({
            depthWrite: false,
            depthTest: false,
            fog: false,
            blending: NoBlending,
            uniforms: { shadow: { type: 't', value: shadowTexture } },
            vertexShader,
            fragmentShader
        }));
        this.matrixAutoUpdate = false;
        this.frustumCulled = false;
        this.castShadow = false;
        this.receiveShadow = false;
        this.renderOrder = 0;
    }
}