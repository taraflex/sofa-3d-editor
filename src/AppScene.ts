import { Background } from 'Backaground';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { TransformControls } from 'three/examples/jsm/controls/TransformControls';
import {
    Box3, GridHelper, Object3D, PerspectiveCamera, Raycaster, Scene, Sprite, Texture, Vector3,
    WebGLRenderer, WebGLRenderTarget
} from 'three';

import { Info, Layers, Lds } from './Lds';

const enum State {
    SelectModel = 1,
    SelectTexture
}

function getPName(o: Object3D) {
    return o.parent.name.slice(1).split('_')[1];
}

function getPoints(o: Object3D) {
    const points: Record<string, Sprite> = Object.create(null);
    o.traverse((o: Sprite) => {
        if (o.name === 'plus') {
            points[getPName(o)] = o;
        }
    });
    return points;
}

function neg(s: string) {
    return s === '-' ? '+' : '-';
}

function worldPos(point: Object3D, t = new Vector3(0, 0, 0)) {
    return point.parent.localToWorld(t);
}

function getTexture(o: Object3D): Texture {
    const t = o?.getObjectByName('main')?.getObjectByProperty('type', 'Mesh')?.['material'].map;
    return t?.['userData'] && t;
}

export class AppScene extends Scene {
    protected readonly grid: GridHelper;
    protected readonly renderer: WebGLRenderer;
    protected readonly camera: PerspectiveCamera;
    protected readonly orbitControl: OrbitControls;
    protected readonly dragControl: TransformControls;
    protected readonly raycaster = new Raycaster();
    protected needRender: boolean = true;
    protected animationFrameId = null;
    protected lastTexture: Texture = null;
    protected _lastObject: Object3D = null;
    protected state: State = State.SelectModel;

    protected readonly renderTarget = new WebGLRenderTarget(32, 32, {
        anisotropy: 0,
        generateMipmaps: false,
        stencilBuffer: false,
        depthBuffer: false
    });

    protected readonly requestRender = () => this.needRender = true;

    constructor(
        protected readonly changeVisible: (v: boolean) => void,
        protected readonly onProgress: (v: number) => void,
        protected readonly onSelect: (v: any) => void,
        protected readonly restore: () => void,
        protected readonly canvas: HTMLCanvasElement,
        clientWidth: number,
        clientHeight: number
    ) {
        super();

        this.add(new Background(this.renderTarget.texture));
        this.add(this.grid = new GridHelper(20, 20))

        const renderer = this.renderer = new WebGLRenderer({ canvas, antialias: true });
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(clientWidth, clientHeight);
        renderer.autoClearStencil = false;
        renderer.autoClearColor = false;

        const camera = this.camera = new PerspectiveCamera(60, clientWidth / clientHeight, 0.1, 100);
        camera.position.set(-0.5, 1, 2);

        const orbitControl = this.orbitControl = new OrbitControls(camera, canvas);
        orbitControl.enableDamping = true;
        orbitControl.dampingFactor = 0.2;
        orbitControl.screenSpacePanning = false;
        orbitControl.minDistance = 1;
        orbitControl.maxDistance = 10;
        orbitControl.maxPolarAngle = Math.PI / 2;
        orbitControl.addEventListener('change', this.requestRender);

        const dragControl = this.dragControl = new TransformControls(camera, canvas);
        dragControl.setMode('translate');
        dragControl.setTranslationSnap(0.2);
        dragControl.showY = false;
        dragControl.setSize(1.4);
        const dragGizmo: any = dragControl.getObjectByProperty('type', 'TransformControlsGizmo');
        [dragGizmo.gizmo, dragGizmo.picker].forEach(t => t
            .translate
            .children
            .filter(o => o.name.indexOf('Y') > -1)
            .forEach(o => o.parent.remove(o))
        );
        dragControl.addEventListener('change', this.requestRender);
        dragControl.addEventListener('dragging-changed', event => {
            orbitControl.enabled = !event.value;
            if (!event.value) {
                this.switchControlls(this.lastObject, true);
            }
        });
        this.add(dragControl);

        this.animate();
    }

    protected findIntersect(o: Object3D, layer: number, recursive?: boolean): Object3D {
        if (!o) {
            return null;
        }
        this.raycaster.layers.set(layer);
        const ints = this.raycaster.intersectObjects(o.children, recursive);
        const i = ints.filter(e => e.object.visible).find(({ distance }, _, arr) => arr.every(e => e.distance >= distance));
        return i?.object;
    }

    protected findTopParent(o: Object3D) {
        return o.parent === this || o.parent === null ? o : this.findTopParent(o.parent);
    }

    protected worldPointDirection(point: Sprite) {
        const p = worldPos(point).sub(this.findTopParent(point.parent).position);
        if (Math.abs(p.z) < 0.4) {
            p.setZ(0);
        }
        if (Math.abs(p.x) < 0.4) {
            p.setX(0);
        }
        return p.setY(0).normalize();
    }

    protected blur() {
        this.lastObject = null;
        this.grid.visible = false;
        this.requestRender();
    }

    protected connectPoints(o: Object3D, point: Sprite) {
        const points = getPoints(o);
        const pnames = Object.keys(points).sort();
        const [type, dir] = getPName(point).split('');
        const k = [
            type + neg(dir),
            type + dir,
            pnames.find(k => k.endsWith(dir)),
            pnames.find(k => k.endsWith(neg(dir))),
        ].find(k => !!points[k]);

        const rotatePoint = points[k];

        const direction = this.worldPointDirection(point);
        let t: Vector3;
        do {
            o.rotateY(Math.PI / 2);
            o.updateMatrixWorld();
            t = this.worldPointDirection(rotatePoint).add(direction);
        } while (t.z !== 0 || t.x !== 0);

        o.position
            .add(worldPos(point).sub(worldPos(rotatePoint)))
            .multiplyScalar(10)
            .round()
            .divideScalar(10);
    }

    protected getLastObjectTexture(): Texture {
        return getTexture(this.lastObject) || this.lastTexture;
    }

    protected focus(o: Object3D, newState?: State) {
        this.state = newState || this.state
        this.lastObject = o;
        switch (this.state) {
            case State.SelectModel: return this.onSelect(o.userData);
            case State.SelectTexture: return this.onSelect(this.getLastObjectTexture()?.['userData'] || { path: [1] });
        }
    }

    protected switchControlls(o: Object3D, visible: boolean) {
        if (o) {
            if (visible) {
                o.updateMatrixWorld();//required for worldPos
            }
            o.traverse((object: Sprite) => {
                if (object.isSprite) {
                    if (visible) {
                        if (object.name === 'plus') {
                            const freePoint = worldPos(object, new Vector3(0, 0, 0.9));
                            /* const m = new Mesh(new BoxBufferGeometry(0.1, 0.1, 0.1), new MeshBasicMaterial({ color: 'red' }));
                             m.position.set(freePoint.x, freePoint.y, freePoint.z);
                             this.add(m);*/
                            for (let c of this.children) {
                                if (c !== o) {
                                    const bb = c.getObjectByName('_bb');
                                    if (bb && new Box3().setFromObject(bb).containsPoint(freePoint)) {
                                        object.visible = false;
                                        return;
                                    }
                                }
                            }
                        } else if (object.name == 'color') {
                            return object.visible = this.state === State.SelectModel;
                        } else if (object.name == 'replace') {
                            return object.visible = this.state === State.SelectTexture;
                        }
                    }
                    object.visible = visible;
                }
            });
        }
    }

    protected findNearestPoint(o: Object3D): Sprite {
        const bb = new Box3().setFromObject(o.getObjectByName('_bb'));

        let minDist = Number.POSITIVE_INFINITY;
        let minPoint: Sprite = null;

        for (let c of this.children) {
            if (c !== o) {
                const points = getPoints(c);
                for (let k in points) {
                    const dist = bb.distanceToPoint(worldPos(points[k]))
                    if (dist === 0) {
                        return points[k];
                    }
                    if (dist < 0.3 && dist < minDist) {
                        minDist = dist;
                        minPoint = points[k];
                    }
                }
            }
        };

        return minPoint;
    }

    protected disposeTextureIfUnused(t: Texture) {
        if (t && t !== this.lastTexture && !this.children.some(o => getTexture(o) === t)) {
            //console.log('before', this.renderer.info)
            this.loader.disposeTexture(t);
            //console.log('after', this.renderer.info)
        }
    }

    public removeCurrent() {
        const { lastObject } = this;
        if (lastObject) {
            const nearest = this.findNearestPoint(lastObject);
            const t = getTexture(lastObject);
            this.blur();
            this.remove(lastObject);
            this.disposeTextureIfUnused(t);
            if (this.children.length < 4) {
                this.orbitControl.reset();
                this.restore();
            } else if (nearest) {
                this.focus(this.findTopParent(nearest));
            } else {
                this.changeVisible(false);
            }
        }
    }

    public readonly onTap = (event) => {
        const rect = this.canvas.getBoundingClientRect();
        const x = (event.detail.x - rect.left) / rect.width * 2 - 1;
        const y = 1 - (event.detail.y - rect.top) / rect.height * 2;

        this.raycaster.setFromCamera({ x, y }, this.camera);
        let o: Object3D;
        if (o = this.findIntersect(this.lastObject, Layers.BUTTONS, true)) {
            if (o.name === 'plus') {
                const cloned = Lds.deepClone(this.findTopParent(o), null);
                this.connectPoints(cloned, o as Sprite);
                this.focus(cloned, State.SelectModel);
            } else if (o.name === 'remove') {
                this.removeCurrent();
            } else if (o.name === 'replace') {
                this.focus(this.findTopParent(o), State.SelectModel);
            } else if (o.name === 'color') {
                this.focus(this.findTopParent(o), State.SelectTexture);
            } else if (o.name === 'rotate') {
                o = this.findTopParent(o);
                o.rotateY(-Math.PI / 2);
                this.focus(o);
            }
        } else if (o = this.findIntersect(this, Layers.BBOX, true)) {
            this.focus(this.findTopParent(o));
        } else {
            this.blur();
            this.changeVisible(false);
        }
        this.requestRender();
    }

    protected set lastObject(o: Object3D) {
        if (o != this._lastObject) {
            this.switchControlls(this._lastObject, false);
            this.dragControl.detach();
            if (o) {
                if (o.parent != this) {
                    this.add(o);
                }
                this.switchControlls(o, true);
                this.dragControl.attach(o);
            }
            this._lastObject = o || null;
            this.grid.visible = !!this._lastObject;
            this.requestRender();
        } else {
            this.switchControlls(o, true);
        }
    }

    protected get lastObject() {
        return this._lastObject;
    }

    protected readonly loader = new Lds((e: ProgressEvent) => {
        const p = (Math.min(e.total, e.loaded) / e.total) * 100;
        this.onProgress(p >= 100 ? -1 : p);
    });

    async setModel(info: Info) {
        if (this.lastObject) {
            if (info.url != this.lastObject.userData.url) {
                const tx = this.getLastObjectTexture();
                const o = await this.loader.loadModel(info, tx);
                o.position.setFromMatrixPosition(this.lastObject.matrix);
                o.rotation.copy(this.lastObject.rotation);
                const npoint = this.findNearestPoint(this.lastObject);
                if (npoint) {
                    this.connectPoints(o, npoint);
                }
                this.remove(this.lastObject);
                this.state = State.SelectModel;
                this.lastObject = o;
            }
        } else {
            this.state = State.SelectModel;
            this.lastObject = await this.loader.loadModel(info, this.lastTexture);
        }
        setTimeout(this.requestRender, 300);
    }

    async setTexture(info: Info) {
        const { lastTexture } = this;
        this.lastTexture = await this.loader.loadTexture(info);
        if (this.lastObject) {
            Lds.appendTexture(this.lastObject, this.lastTexture);
            this.requestRender();
        }
        this.state = State.SelectTexture;
        this.disposeTextureIfUnused(lastTexture);
        setTimeout(this.requestRender, 300);
    }

    protected render() {
        this.renderer.setRenderTarget(this.renderTarget);
        this.renderer.clearColor();
        this.camera.layers.set(Layers.SHADOW);
        this.renderer.render(this, this.camera);

        this.renderer.setRenderTarget(null);
        this.camera.layers.set(0);
        this.renderer.render(this, this.camera);

        this.needRender = false;
    }

    protected readonly animate = () => {
        this.animationFrameId = requestAnimationFrame(this.animate);
        if (this.orbitControl.update() || this.needRender) {
            this.render();
        }
    }

    resize(clientWidth: number, clientHeight: number) {
        this.camera.aspect = clientWidth / clientHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(clientWidth, clientHeight);
        this.render();
    }

    dispose() {
        //todo remove all event listeners
        this.needRender = false;
        window.cancelAnimationFrame(this.animationFrameId);
        this.orbitControl.dispose();
        this.dragControl.dispose();
        super.dispose();
    }
}