const { DefinePlugin } = require('webpack');
const { basename, resolve } = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');
const { argv } = require('yargs');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const { DuplicatesPlugin } = require('inspectpack/plugin');
//const ManifestPlugin = require('webpack-manifest-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { version, name: appname } = require('./package.json');
const CopyPlugin = require('copy-webpack-plugin');

const DEV_SERVER = basename(require.main.filename) === 'webpack-dev-server.js';
const DEV = !!(DEV_SERVER || argv.watch || (process.env.NODE_ENV && process.env.NODE_ENV.startsWith('dev')) || (argv.mode && argv.mode.toString().startsWith('dev')));

const extensions = ['.mjs', '.js', '.ts', '.jsx', '.tsx'];

const u = (...a) => a.filter(Boolean);

const tsLoader = {
    loader: 'ts-loader',
    options: {
        compilerOptions: {
            sourceMap: DEV,
            target: DEV ? 'es2018' : 'es5',
            noUnusedLocals: !DEV,
            noUnusedParameters: !DEV
        }
    }
}

const babelLoader = {
    loader: 'babel-loader',
    options: {
        presets: [
            ['@babel/preset-env', { modules: false /*, useBuiltIns: 'entry', corejs: 3*/ }]
        ]
    }
}

const cssLoader = {
    loader: 'css-loader',
    options: {
        sourceMap: DEV
    }
}
const sassLoader = {
    loader: 'sass-loader',
    options: {
        sourceMap: DEV
    }
}
const postcssLoader = DEV ? undefined : {
    loader: 'postcss-loader',
    options: {
        plugins: [
            require('postcss-flexbugs-fixes'),
            require('autoprefixer')
        ],
        sourceMap: DEV
    }
}
const styleLoader = DEV ? 'style-loader' : MiniCssExtractPlugin.loader;

const nameTemplate = (ext = 'js') => `[name].${ext}?h=[contenthash:7]`;

module.exports = {
    entry: {
        index: './src/index.ts',
    },
    devServer: DEV_SERVER ? {
        contentBase: resolve(__dirname, 'build'),
        compress: false,
        port: 9095,
    } : undefined,
    target: 'web',
    devtool: DEV ? 'cheap-module-eval-source-map' : undefined,
    mode: DEV ? 'development' : 'production',
    watch: DEV_SERVER || argv.watch,
    output: {
        path: resolve(__dirname, 'build'),
        devtoolModuleFilenameTemplate: '[absolute-resource-path]',
        filename: nameTemplate(),
        chunkFilename: nameTemplate()
    },
    node: {
        process: DEV,
        setImmediate: false,
        Buffer: false,
        fs: 'empty'
    },
    resolve: {
        extensions,
        alias: {
            //'stream-browserify': 'nop',
            //'readable-stream': 'nop'
        },
        plugins: [new TsconfigPathsPlugin({ extensions })],
    },
    performance: { hints: false },
    optimization: {
        minimize: !DEV,
        minimizer: [
            new TerserPlugin({
                extractComments: false,
                terserOptions: {
                    ecma: 5,
                    compress: {
                        collapse_vars: false//очень часто ломает скрипты при минификации - лучше отключить
                    },
                    output: {
                        comments: false
                    }
                }
            }),
            new OptimizeCssAssetsPlugin({
                assetNameRegExp: /\.css(\?h=\w+)?$/,
                cssProcessorPluginOptions: {
                    preset: ['default', { discardComments: { removeAll: true }, zindex: false }],
                }
            })
        ]
    },
    plugins: u(
        new DefinePlugin({
            APP_NAME: JSON.stringify(appname),
            APP_VERSION: JSON.stringify(version),
            'process.env.BROWSER': true,
            'process.env.NODE_ENV': JSON.stringify(DEV ? 'development' : 'production'),
            'process.env.DEBUG': DEV
        }),
        DEV ? new DuplicatesPlugin() : 0,
        DEV ? new CircularDependencyPlugin({
            exclude: /node_modules/,
            allowAsyncCycles: true
        }) : 0,
        DEV ? 0 : new MiniCssExtractPlugin({
            filename: nameTemplate('css')
        }),
        DEV ? 0 : new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            openAnalyzer: false,
            reportFilename: 'build-report.htm'
        }),
        new HtmlWebpackPlugin({
            template: './src/template.html',
            chunks: ['index']
        }),
        new CopyPlugin([
            { from: 'src/*.json', flatten: true },
            { from: 'src/models/*.FBX', to: __dirname + '/build/models/', flatten: true },
        ]),
        /*new ManifestPlugin({
            fileName: 'assets.php',
            writeToFileEmit: true,
            //filter: ({ name }) => name.includes('index'),
            serialize: manifest => `<?php 
function assets(){
    return [
${Object.keys(manifest).map(key => `'${key}'=>'${manifest[key]}'`).join(',\n')}
    ];
}?>`
        })*/
    ),
    module: {
        rules: u({
            test: /\.scss$/,
            use: u(styleLoader, cssLoader, postcssLoader, sassLoader)
        }, {
            test: /\.css$/,
            use: u(styleLoader, cssLoader, postcssLoader)
        }, {
            test: /\.(t|j)sx?$/,
            use: tsLoader,
            exclude: DEV ? [/node_modules/] : [/core-js/]
        }, DEV ? 0 : {
            test: /\.mjsx?$/,
            use: babelLoader
        }, {
            test: /\.svelte$/,
            use: u(DEV ? 0 : babelLoader, {
                loader: 'svelte-loader',
                options: {
                    emitCss: !DEV,
                },
            })
        }, {
            test: /\.pug$/,
            loader: 'pug-loader',
            options: {
                pretty: DEV
            }
        }, {
            test: /\.(png|jpe?g|gif|webp)$/,
            loader: 'url-loader',
            options: {
                limit: 4096,
                name: '[name].[md5:hash:base58:7].[ext]',
                outputPath: 'assets/'
            }
        }, {
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            loader: 'url-loader',
            options: {
                limit: 8192,
                name: '[md5:hash:base58:7].[ext]',
                outputPath: 'assets/'
            }
        }, {
            test: /\.(txt|glsl)$/,
            use: 'raw-loader',
        })
    },
    stats: {
        hash: false,
        modules: false,
        version: false,
        children: false,
        assets: false,
        colors: true,
        depth: false,
        performance: false
    }
}